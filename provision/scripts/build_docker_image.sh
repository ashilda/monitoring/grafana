#!/bin/sh

IMAGE_TAG=$DOCKER_REGISTRY/$DOCKER_NAMESPACE/$DOCKER_IMAGE:$DOCKER_VERSION-$BUILD_VERSION

check_for_error() {
  if [[ $? -ne 0 ]]; then
    echo "ERROR: Failed to "$1
    exit 1
  fi
}

docker build --tag $IMAGE_TAG .
check_for_error "build image"

docker login -u $REGISTRY_LOGIN -p $REGISTRY_PASSWORD $DOCKER_REGISTRY
check_for_error "login"

docker push $IMAGE_TAG
check_for_error "push"